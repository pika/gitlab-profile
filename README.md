Use PIKA [packages](https://gitlab.hrz.tu-chemnitz.de/pika/packages) to install PIKA on your cluster.

# Publications

R. Dietrich, F. Winkler, A. Knüpfer and W. Nagel: PIKA: Center-Wide and Job-Aware Cluster Monitoring. Proc. HPCMASPA 2020, Workshop on Monitoring and Analysis for High Performance Computing Systems Plus Applications, September 14, 2020, Kobe, Japan. Held in conjunction with IEEE Cluster 2020. DOI: [10.1109/CLUSTER49012.2020.00061](https://ieeexplore.ieee.org/document/9229636)

Winkler, F., Knüpfer, A. (2023). Automatic Detection of HPC Job Inefficiencies at TU Dresden’s HPC Center with PIKA. In: Bienz, A., Weiland, M., Baboulin, M., Kruse, C. (eds) High Performance Computing. ISC High Performance 2023. Lecture Notes in Computer Science, vol 13999. Springer, Cham. [https://doi.org/10.1007/978-3-031-40843-4_22](https://doi.org/10.1007/978-3-031-40843-4_22)
